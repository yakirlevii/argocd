```
kubectl apply -n cicd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml  
```

```
kubectl patch svc argocd-server -n cicd --type='json' -p='[{"op": "replace", "path": "/spec/type", "value": "LoadBalancer"}]'  
```

```
kubectl -n cicd get secret argocd-initial-admin-secret -o jsonpath='{.data.password}'  
```

```
http://<IP-address or server-name>:80  
```  

## install using helm  
```  
kubectl create namespace argocd  
helm repo add argo https://argoproj.github.io/argo-helm  
helm repo update  
helm install argocd argo/argo-cd -n argocd  
```  
```  
kubectl get secret --namespace argocd argocd-initial-admin-secret -o jsonpath="{.data.password}"  
kubectl --namespace argocd port-forward svc/argo-cd-argocd-server 8080:443  
```  
